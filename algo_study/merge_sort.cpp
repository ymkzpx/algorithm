#include <iostream>
#include <string>
#include <sstream>
#include <math.h>
#include <map>
#include <queue>
#include <vector>
#include <set>
#include <stack>
#include <algorithm>
#include <unordered_map>

using namespace std;

#define rep(i, a, b) for (int i=(a);i<=(b);i++)
#define per(i, a, b) for (int i=(b);i>=(a);i--)
#define all(x) (x).begin(),(x).end()
#define SZ(x) ((int)(x).size())
#define PII pair<int,int>
#define VI vector<int>
#define ll long long
#define db double;
#define pb push_back
#define mp make_pair
#define fi first
#define se second
// head

const int maxn = 2e5 + 7;

int a[maxn], b[maxn];

void merge_sort(int l, int r) {
	if(l >= r) return;
	int mid = (l + r) / 2;
	merge_sort(l, mid);
	merge_sort(mid + 1, r);
	int fi = l, se = mid + 1, pos = l - 1;
	while(fi <= mid && se <= r) {
		if(a[fi] <= a[se]) b[++pos] = a[fi], ++fi;
		else b[++pos] = a[se], ++se;
	}
	while(fi <= mid) b[++pos] = a[fi], ++fi;
	while(se <= r) b[++pos] = a[se], ++se;
	for(int i = l; i <= r; i++) a[i] = b[i];
}

int main() {
	int n;
	scanf("%d", &n);
	for(int i = 1; i <= n; i++) scanf("%d", &a[i]);
	merge_sort(1, n);
	for(int i = 1; i <= n; i++) cout << a[i] << ' ';
}

