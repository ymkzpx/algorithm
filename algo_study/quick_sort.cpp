#include <iostream>
#include <string>
#include <sstream>
#include <math.h>
#include <map>
#include <queue>
#include <vector>
#include <set>
#include <stack>
#include <algorithm>
#include <unordered_map>

using namespace std;

#define rep(i, a, b) for (int i=(a);i<=(b);i++)
#define per(i, a, b) for (int i=(b);i>=(a);i--)
#define all(x) (x).begin(),(x).end()
#define SZ(x) ((int)(x).size())
#define PII pair<int,int>
#define VI vector<int>
#define ll long long
#define db double;
#define pb push_back
#define mp make_pair
#define fi first
#define se second
// hea

// 快速排序

class Solution {
public:
	void sortA(int l, int r, vector<int>& a) {
		if(l >= r) return;
		int fi = l, se = r, key = a[l];
		while(fi < se) {
			while(fi < se && a[se] >= key) --se;
			a[fi] = a[se];
			while(fi < se && a[fi] <= key) ++fi;
			a[se] = a[fi];
		} 
		a[fi] = key;
		sortA(l, fi - 1, a);
		sortA(fi + 1, r, a);
	}
    vector<int> sortArray(vector<int>& nums) {
    	sortA(0, int(nums.size()) - 1, nums);
    	return nums;
    }
};

int main() {
    int n, m;
    while(scanf("%d %d", &n, &m) != EOF) {
        vector<int> a(n);
        rep(i, 0, n - 1) scanf("%d", &a[i]);
        Solution *now = new Solution();
        vector<int> ans = now->sortArray(a);
        //for(int i = 0; i < ans)
        for(int i = n - 1; i > n - m - 1; i--) {
            printf("%d%c", ans[i], i == n - m ? '\n' : ' ');
        }
    }
}
