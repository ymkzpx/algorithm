#include <iostream>
#include <string>
#include <sstream>
#include <math.h>
#include <map>
#include <queue>
#include <vector>
#include <set>
#include <stack>
#include <algorithm>
#include <unordered_map>

using namespace std;

#define rep(i, a, b) for (int i=(a);i<=(b);i++)
#define per(i, a, b) for (int i=(b);i>=(a);i--)
#define all(x) (x).begin(),(x).end()
#define SZ(x) ((int)(x).size())
#define PII pair<int,int>
#define VI vector<int>
#define ll long long
#define db double;
#define pb push_back
#define mp make_pair
#define fi first
#define se second
// head

struct ListNode {
  int val;
  ListNode *next;
  ListNode(int x) : val(x), next(NULL) {}
};

class Solution {
public:
    ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
    	if(l1 == NULL && l2 == NULL) return NULL;
    	if(l1 != NULL && l2 == NULL) return l1;
    	if(l2 != NULL && l1 == NULL) return l2;
    	ListNode* ans, *last, *fi, *se;

    	if(l1->val <= l2->val) ans = l1, fi = l1->next, se = l2;
    	else ans = l2, fi = l1, se = l2->next;
    	last = ans;
    	while(fi != NULL && se!= NULL) {
    		if(fi->val <= se->val) {
    			last->next = fi;
    			last = fi;
    			fi = fi->next;
    		} else {
    			last->next = se;
    			last = se;
    			se = se->next;
    		}
    	}
    	if(fi != NULL) last->next = fi;
    	if(se != NULL) last->next = se;
    	return ans;
    }
};